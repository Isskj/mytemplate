import UIKit

protocol SignupViewAction: AnyObject {
    func signupViewAction(user: UserModel, completion: ((Error?) -> Void)?)
}

class SignupView: UIView, ViewComponent {
    typealias MODEL = UserModel
    typealias DELEGATE = SignupViewAction

    weak var delegate: SignupViewAction?

    @IBOutlet private var idInputField: UITextField!
    @IBOutlet private var passInputField: UITextField!
    @IBOutlet private var nameInputField: UITextField!
    @IBOutlet private var ageInputField: UITextField!

    func updateUI(model: UserModel) {
        self.idInputField?.text = model.email
        self.passInputField?.text = model.password
        self.nameInputField?.text = model.name
        self.ageInputField?.text = "\(model.age ?? 0)"
    }

    @IBAction func tapSignup(button: UIButton) {
        self.resignAllResponders()

        var user = UserModel()
        user.email = self.idInputField.text
        user.password = self.passInputField.text
        user.name = self.nameInputField.text
        user.age = Int(self.ageInputField.text ?? "")

        button.isEnabled = false
        self.delegate?.signupViewAction(user: user) { _ in
            button.isEnabled = true
        }
    }
}
