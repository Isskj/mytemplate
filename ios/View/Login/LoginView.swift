import UIKit

protocol LoginViewAction: AnyObject {
    func loginViewAction(user: UserModel, completion: ((Error?) -> Void)?)
}

class LoginView: UIView, ViewComponent {
    typealias MODEL = UserModel
    typealias DELEGATE = LoginViewAction

    weak var delegate: LoginViewAction?

    @IBOutlet private var idInputField: UITextField!
    @IBOutlet private var passInputField: UITextField!

    func updateUI(model: UserModel) {
        self.idInputField?.text = model.email
        self.passInputField?.text = model.password
    }

    @IBAction func tapLogin(button: UIButton) {
        self.resignAllResponders()

        var user = UserModel()
        user.email = self.idInputField.text
        user.password = self.passInputField.text

        button.isEnabled = false
        self.delegate?.loginViewAction(user: user) { _ in
            button.isEnabled = true
        }
    }
}
