import DefaultsKit
import Moya
import RxSwift
import UIKit

class SignupController: UIViewController {
    let dispossalBag = DisposeBag()

    var coordinator: AuthCoordinator?

    @IBOutlet private var signupContainer: UIView! {
        didSet {
            SignupView.attach(parent: self.signupContainer)?.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension SignupController: SignupViewAction {
    func signupViewAction(user: UserModel, completion: ((Error?) -> Void)?) {
        let provider = MoyaProvider<APISource>(plugins: [NetworkLoggerPlugin()])

        provider.rx.request(.signup(user))
            .filterAPI()
            .map(UserModel.self)
            .subscribe(onSuccess: { response in
                if let name = response.email {
                    Defaults.shared.set(name, for: .userID)
                    self.coordinator?.pageTop()
                } else {
                    fatalError("user not registered")
                }
                log.debug("success :\(response)")
                completion?(nil)
            }, onError: { error in
                log.error(error)
                completion?(error)
            }).disposed(by: self.dispossalBag)
    }
}
