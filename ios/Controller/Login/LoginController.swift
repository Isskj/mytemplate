import Moya
import RxSwift
import UIKit

class LoginController: UIViewController {
    let dispossalBag = DisposeBag()

    var coordinator: AuthCoordinator?

    @IBOutlet private var loginContainer: UIView! {
        didSet {
            LoginView.attach(parent: self.loginContainer)?.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension LoginController: LoginViewAction {
    func loginViewAction(user: UserModel, completion: ((Error?) -> Void)?) {
        let provider = MoyaProvider<APISource>(plugins: [NetworkLoggerPlugin()])
        provider.rx.request(.login(user))
            .filterAPI()
            .map(UserModel.self)
            .subscribe(onSuccess: { response in
                log.debug("success :\(response)")
                completion?(nil)
            }, onError: { error in
                log.error(error)
                completion?(error)
            }).disposed(by: self.dispossalBag)
    }
}
