import UIKit

class TopCoordinator: Coordinator {
    weak var parent: UINavigationController?
    weak var window: UIWindow?

    let controller: UIViewController

    init() {
        self.controller = UIStoryboard(name: "TopController", bundle: Bundle.main).instantiateInitialViewController()!
    }

    func start() {
        if let win = self.window {
            win.rootViewController = self.controller
            win.makeKeyAndVisible()
        }
    }
}
