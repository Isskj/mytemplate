import UIKit

class AppCoordinator: Coordinator {
    weak var parent: UINavigationController?
    weak var window: UIWindow?

    private var childCoordinators = [Coordinator]()

    init() {}

    func start() {
        let auth = AuthCoordinator()
        auth.parent = self.parent
        auth.window = self.window
        auth.start()
        self.childCoordinators.append(auth)
    }
}
