import DefaultsKit
import UIKit

class AuthCoordinator: Coordinator {
    weak var parent: UINavigationController?
    weak var window: UIWindow?

    let controller: UIViewController

    init() {
        self.controller = UIStoryboard(name: "LoginController", bundle: Bundle.main).instantiateInitialViewController()!
    }

    func start() {
        if let win = self.window {
            if Defaults.shared.has(.userID) {
                win.rootViewController = UIStoryboard(name: "LoginController", bundle: Bundle.main)
                    .instantiateInitialViewController()
            } else {
                win.rootViewController = UIStoryboard(name: "SignupController", bundle: Bundle.main)
                    .instantiateInitialViewController()
            }
            win.rootViewController = self.controller
            win.makeKeyAndVisible()
        }
    }

    func pageTop() {
        if let win = self.window {
            win.rootViewController = UIStoryboard(name: "TopController", bundle: Bundle.main).instantiateInitialViewController()
            win.makeKeyAndVisible()
        }
    }
}
