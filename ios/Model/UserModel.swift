import Foundation

struct UserModel: Codable, ObjectPrintable {
    var email: String?
    var password: String?
    var name: String?
    var age: Int?

    enum CodingKeys: CodingKey {
        case email
        case password
        case name
        case age
    }
}
