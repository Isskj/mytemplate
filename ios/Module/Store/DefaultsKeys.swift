import DefaultsKit
import Foundation

extension DefaultsKey {
    static let userID = Key<String>("user.id")
}
