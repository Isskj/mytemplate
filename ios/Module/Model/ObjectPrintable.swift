import Foundation

protocol ObjectPrintable: Encodable {
    var debugDescription: String { get }
}

extension ObjectPrintable {
    var debugDescription: String {
        if let data = try? JSONEncoder().encode(self) {
            return String(data: data, encoding: .utf8) ?? ""
        }
        return ""
    }
}
