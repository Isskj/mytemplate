import UIKit

protocol ViewComponent: UIView {
    associatedtype MODEL
    associatedtype DELEGATE
    var delegate: DELEGATE? { get set }
    func updateUI(model: MODEL)
}

extension ViewComponent {
    static func attach(parent: UIView) -> Self? {
        if let view = UINib(nibName: String(describing: self),
                            bundle: Bundle.main)
            .instantiate(withOwner: parent, options: nil).first as? UIView {
            parent.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: 0).isActive = true
            view.trailingAnchor.constraint(equalTo: parent.trailingAnchor, constant: 0).isActive = true
            view.topAnchor.constraint(equalTo: parent.topAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: 0).isActive = true
            return view as? Self
        }
        return nil
    }

    func resignAllResponders() {
        for view in self.subviews {
            view.resignFirstResponder()
        }
    }
}
