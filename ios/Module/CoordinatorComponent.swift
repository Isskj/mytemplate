import UIKit

protocol Coordinator: AnyObject {
    var parent: UINavigationController? { get set }
    var window: UIWindow? { get set }
    func start()
}
