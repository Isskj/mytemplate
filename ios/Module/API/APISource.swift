import Foundation
import Moya
import RxSwift

enum APISource {
    case signup(UserModel), login(UserModel), logout(UserModel)
}

extension APISource: TargetType {
    var baseURL: URL {
        return URL(string: "http://localhost:12345/api")!
    }

    var path: String {
        switch self {
        case .signup:
            return "/auth/signup"
        case .login:
            return "/auth/login"
        case .logout:
            return "/auth/logout"
        }
    }

    var method: Moya.Method {
        switch self {
        case .signup, .login, .logout:
            return .post
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case let .signup(user):
            log.debug(user)
            return .requestJSONEncodable(user)
        case let .login(user):
            log.debug(user)
            return .requestJSONEncodable(user)
        case let .logout(user):
            log.debug(user)
            return .requestJSONEncodable(user)
        }
    }

    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}
