import Moya
import RxSwift

enum APIError: Swift.Error {
    case general(String)
}

extension PrimitiveSequence where Trait == SingleTrait, Element == Response {
    func filterAPI() -> Single<Element> {
        return flatMap { (res: Response) in
            do {
                return .just(try res.filterSuccessfulStatusCodes())
            } catch {
                let error = String(data: res.data, encoding: .utf8)
                throw APIError.general("error:\(String(describing: error))")
            }
        }
    }
}
