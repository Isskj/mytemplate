# Android Sample

[https://github.com/android/sunflower](https://github.com/android/sunflower)

Modified:
  * Replace ``Room`` persistent library with ``Realm`` database library
  * Replace ``NavHost`` with ``Coordinator`` (with ``MVC+Coordinator`` architecture) 
  * Change design architecture to ``MVC+Coordinator``
  * Remove ``Repository``, ``Dao`` design pattern because it's redundant

