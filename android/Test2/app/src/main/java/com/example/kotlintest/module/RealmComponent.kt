package com.example.kotlintest.module

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import io.realm.RealmChangeListener
import io.realm.RealmModel
import io.realm.RealmResults
import io.realm.kotlin.addChangeListener
import io.realm.kotlin.isValid
import io.realm.kotlin.removeChangeListener
import java.text.SimpleDateFormat
import java.util.*

class RealmComponent: BaseModule {

    companion object {
        @SuppressLint("SimpleDateFormat")
        val format = SimpleDateFormat("yyyy/MM/dd")
        fun dateString(date: Date): String {
            return format.format(date)
        }
    }
    override fun dispose() {
    }
}

class RealmResultsLiveData<T : RealmModel>(
    private val results: RealmResults<T>
) : LiveData<RealmResults<T>>() {

    private val listener = RealmChangeListener<RealmResults<T>> { results -> value = results }

    override fun onActive() {
        super.onActive()
        if (results.isValid) {
            results.addChangeListener(listener)
            value = results
        }
    }

    override fun onInactive() {
        super.onInactive()
        if (results.isValid) {
            results.removeChangeListener(listener)
        }
    }
}

class RealmResultLiveData<T : RealmModel>(
    private val result: T
) : LiveData<T>() {

    private val listener = RealmChangeListener<T> { result -> value = result }

    override fun onActive() {
        super.onActive()
        if (result.isValid()) {
            result.addChangeListener(listener)
            value = result
        }
    }

    override fun onInactive() {
        super.onInactive()
        if (result.isValid()) {
            result.removeChangeListener(listener)
        }
    }
}

fun <T : RealmModel> RealmResults<T>.asLiveData() = RealmResultsLiveData<T>(this)
fun <T : RealmModel> T.asLiveData() = RealmResultLiveData(this)
