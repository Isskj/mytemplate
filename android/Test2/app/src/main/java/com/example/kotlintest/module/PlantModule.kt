package com.example.kotlintest.module

import android.content.Context
import com.example.kotlintest.model.GardenPlanting
import com.example.kotlintest.model.Plant
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.operators.observable.ObservableJust
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmQuery
import io.realm.RealmResults
import io.realm.kotlin.where
import io.realm.rx.RxObservableFactory
import kotlinx.coroutines.flow.asFlow
import java.util.*

class PlantModule: BaseModule {

    var zone = false

    val disposals = CompositeDisposable()

    fun getPlants(observer: SingleObserver<List<Plant>>) {
        val listener = RealmChangeListener<RealmResults<Plant>> { result ->
            Single.just(result.toMutableList()).subscribe(observer)
            result.removeAllChangeListeners()
        }
        if (zone) {
            Realm.getDefaultInstance()
                .where<Plant>()
                .equalTo("growZoneNumber", 9 as Int)
                .findAllAsync()
                .addChangeListener(listener)
        } else {
            Realm.getDefaultInstance()
                .where<Plant>()
                .findAllAsync()
                .addChangeListener(listener)
        }
    }

    fun getPlant(plantId: String, observer: SingleObserver<Pair<Plant, Boolean>>) {
        val listener = RealmChangeListener<Plant> { result ->
            result.removeAllChangeListeners()
            val count = Realm.getDefaultInstance().where<GardenPlanting>()
                .equalTo("plant.plantId", result.plantId)
                .count() > 0
            Single.just(Pair(result, count)).subscribe(observer)
        }
        Realm.getDefaultInstance()
            .where<Plant>()
            .equalTo("plantId", plantId)
            .findFirstAsync()
            .addChangeListener(listener)
    }

    fun isPlanted(plantId: String): Boolean {
        return Realm.getDefaultInstance()
            .where<GardenPlanting>()
            .equalTo("plant.plantId", plantId)
            .count() > 0
    }

    fun addPlantToGarden(plant: Plant) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        println(plant)
        val obj = GardenPlanting(
            plant = plant,
            plantDate = RealmComponent.dateString(Calendar.getInstance().time)
        )
        realm.insert(obj)
        realm.commitTransaction()
    }

    fun getGardenPlants(observer: SingleObserver<List<GardenPlanting>>) {
        val listener = RealmChangeListener<RealmResults<GardenPlanting>> { result ->
            Single.just(result.toMutableList()).subscribe(observer)
            result.removeAllChangeListeners()
        }
        return Realm.getDefaultInstance()
            .where<GardenPlanting>()
            .findAllAsync()
            .addChangeListener(listener)
    }

    fun hasPlants(): Boolean {
        return Realm.getDefaultInstance()
            .where<Plant>().count() > 0L
    }

    fun addDefaultPlantsIfNeeded(context: Context, closure: () -> Unit) {
        if (hasPlants()) {
            return
        }
        context.assets.open("plants.json")
            .use { inputStream ->
                JsonReader(inputStream.reader()).use { reader ->
                    val type = object : TypeToken<List<Plant>>() {}.type
                    val list = Gson().fromJson<List<Plant>>(reader, type)
                    Flowable.just(list)
                }
            }
            .subscribeOn(Schedulers.io())
            .concatMap { list ->
                val realm = Realm.getDefaultInstance()
                realm.beginTransaction()
                realm.delete(Plant::class.java)
                realm.insert(list)
                realm.commitTransaction()
                Flowable.just(true)
            }
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                println("plants $it")
                closure()
            }, {
                closure()
                it.printStackTrace()
            }).also { disposals.add(it) }
    }

    override fun dispose() {
        disposals.dispose()
    }
}