package com.example.kotlintest.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Plant(
    @PrimaryKey
    var pid: String = UUID.randomUUID().toString(),
    var plantId: String = "",
    var name: String = "",
    var description: String = "",
    var growZoneNumber: Int = 0,
    var wateringInterval: Int = 0,
    var imageUrl: String = ""
) : RealmObject()

