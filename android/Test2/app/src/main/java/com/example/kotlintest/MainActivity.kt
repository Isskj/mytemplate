package com.example.kotlintest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlintest.coordinator.AppCoordinator

class MainActivity : AppCompatActivity() {

    private val coordinator: AppCoordinator?
        get() = (application as? MainApplication)?.appCoordinator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        coordinator?.start(this, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        coordinator?.stateComponent?.save(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        coordinator?.stateComponent?.restore(savedInstanceState)
    }
}
