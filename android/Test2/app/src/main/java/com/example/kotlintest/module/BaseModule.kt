package com.example.kotlintest.module

interface BaseModule {
    fun dispose()
}