package com.example.kotlintest.controller

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.kotlintest.R
import com.example.kotlintest.databinding.FragmentPlantDetailBinding
import com.example.kotlintest.model.Plant
import com.example.kotlintest.module.PlantModule
import com.google.android.material.snackbar.Snackbar
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class PlantDetailFragment : BaseFragment() {

    interface Callback {
        fun add(plant: Plant)
    }

    private val disposal = CompositeDisposable()
    private val plantModule = PlantModule()
    private lateinit var binding: FragmentPlantDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlantDetailBinding.inflate(
            inflater, container, false
        ).apply {

            var isToolbarShown = false
            plantDetailScrollview.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
                    val shouldShowToolbar = scrollY > toolbar.height

                    if (isToolbarShown != shouldShowToolbar) {
                        isToolbarShown = shouldShowToolbar
                        appbar.isActivated = shouldShowToolbar
                        toolbarLayout.isTitleEnabled = shouldShowToolbar
                    }
                }
            )
            toolbar.setNavigationOnClickListener {
                coordinator?.pageBack()
            }
            toolbar.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_share -> createShareIntent()
                    else -> false
                }
            }
            setHasOptionsMenu(true)
        }
        binding.callback = object : Callback {
            override fun add(plant: Plant) {
                plantModule.addPlantToGarden(plant)
                binding.isPlanted = true
                Snackbar.make(binding.root, R.string.added_plant_to_garden, Snackbar.LENGTH_LONG)
                    .show()
            }
        }
        subscribe()
        return binding.root
    }

    private fun createShareIntent(): Boolean {
        val act = activity ?: return false
        val shareText = binding.plant?.let {
            getString(R.string.share_text_plant, it.name)
        } ?: return false

        val intent = ShareCompat.IntentBuilder.from(act)
            .setText(shareText)
            .setType("text/plain")
            .createChooserIntent()
            .addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        startActivity(intent)
        return true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        coordinator?.stateComponent?.state?.let { state ->
            val plantId = arguments?.getString("plantId") ?: ""
            state["plantId"] = plantId
        }
    }

    override fun onDetach() {
        coordinator?.stateComponent?.state?.remove("plantId")
        super.onDetach()
    }

    private fun subscribe() {
        val plantId = arguments?.getString("plantId") ?: return
        plantModule.getPlant(plantId, object : SingleObserver<Pair<Plant, Boolean>> {
            override fun onSuccess(it: Pair<Plant, Boolean>) {
                binding.plant = it.first
                binding.isPlanted = it.second
            }
            override fun onSubscribe(d: Disposable) {
                disposal.add(d)
            }
            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })
    }
}