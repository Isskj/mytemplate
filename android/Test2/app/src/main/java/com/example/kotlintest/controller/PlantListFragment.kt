package com.example.kotlintest.controller

import android.os.Bundle
import android.view.*
import com.example.kotlintest.R
import com.example.kotlintest.controller.adapter.PlantListAdapter
import com.example.kotlintest.controller.adapter.PlantListAdapter.OnItemClickListener
import com.example.kotlintest.databinding.FragmentPlantListBinding
import com.example.kotlintest.model.Plant
import com.example.kotlintest.module.PlantModule
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


class PlantListFragment : BaseFragment(), OnItemClickListener {

    private val disposal = CompositeDisposable()
    private val plantModule = PlantModule()
    private lateinit var binding: FragmentPlantListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlantListBinding.inflate(inflater, container, false)
        binding.plantList.adapter = PlantListAdapter(this)
        subscribe()
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposal.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_plant_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter_zone -> {
                plantModule.zone = plantModule.zone.not()
                subscribe()
                return true
            }
            else -> false
        }
    }

    override fun onItemClick(plant: Plant) {
        coordinator?.pagePlantDetail(plant.plantId)
    }

    private fun subscribe() {
        plantModule.getPlants(object : SingleObserver<List<Plant>>{
            override fun onSuccess(list: List<Plant>) {
                (binding.plantList.adapter as? PlantListAdapter)?.submitList(list)
            }
            override fun onSubscribe(d: Disposable) {
                disposal.add(d)
            }
            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })
    }
}

