package com.example.kotlintest.module

import android.os.Bundle

// This class is not persistent.
// It only affects when a device orientation changes. ( So it might be redundant. )
// If you need to handle an persistent data state, use an internal storage.
class SavedStateComponent: BaseModule {
    interface Callback {
        fun onSaved(bundle: Bundle?)
        fun onRestored(bundle: Bundle?)
    }

    val state: MutableMap<String, String> = mutableMapOf()

    var callback: Callback? = null

    fun save(outState: Bundle) {
        val bundle = Bundle()
        state.forEach { (key, value) ->
            bundle.putString(key, value)
        }
        outState.putBundle("appState", bundle)
        callback?.onSaved(outState)
    }

    fun restore(savedInstanceState: Bundle) {
        state.clear()
        (savedInstanceState.get("appState") as? Bundle)?.let { bundle ->
            bundle.keySet().forEach { key ->
                state[key] = bundle[key] as String
            }
        }
        callback?.onRestored(savedInstanceState)
    }

    override fun dispose() {
        state.clear()
        callback = null
    }
}