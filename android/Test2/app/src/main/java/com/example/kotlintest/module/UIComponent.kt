package com.example.kotlintest.module

import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.kotlintest.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

class UIComponent: BaseModule {
    override fun dispose() {
    }
}

@BindingAdapter("imageFromUrl")
fun bindImageUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(imageUrl)
            .into(view)
    }
}

@BindingAdapter("isGone")
fun bindIsGone(view: FloatingActionButton, isGone: Boolean?) {
    val params = view.layoutParams as? CoordinatorLayout.LayoutParams
    val behavior = params?.behavior as? FloatingActionButton.Behavior
    behavior?.isAutoHideEnabled = false
    if (isGone == null || isGone) {
        view.hide()
    } else {
        view.show()
    }
}

@BindingAdapter("isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    view.visibility = if (isGone) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("renderHtml")
fun bindRenderHtml(view: TextView, description: String?) {
    description?.let {
        view.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
        view.movementMethod = LinkMovementMethod.getInstance()
    } ?: run {
        view.text = ""
    }
}

@BindingAdapter("wateringText")
fun bindWateringText(view: TextView, wateringInterval: Int) {
    view.resources?.let { res ->
        val text = res.getQuantityString(
            R.plurals.watering_needs_suffix,
            wateringInterval,
            wateringInterval
        )
        view.text = text
    }
}
