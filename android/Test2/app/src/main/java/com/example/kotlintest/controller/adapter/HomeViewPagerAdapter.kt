package com.example.kotlintest.controller.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.kotlintest.controller.GardenPlantListFragment
import com.example.kotlintest.controller.HomeViewPagerFragment.Companion.GARDEN_LIST
import com.example.kotlintest.controller.HomeViewPagerFragment.Companion.PLANT_LIST
import com.example.kotlintest.controller.PlantListFragment
import java.lang.IndexOutOfBoundsException

class HomeViewPagerAdapter(
    fragment: Fragment
) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            GARDEN_LIST -> GardenPlantListFragment()
            PLANT_LIST -> PlantListFragment()
            else -> throw IndexOutOfBoundsException("")
        }
    }
}