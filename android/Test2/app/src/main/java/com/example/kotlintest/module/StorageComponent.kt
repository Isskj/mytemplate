package com.example.kotlintest.module

import android.content.Context

class StorageComponent: BaseModule {
    val PREF_APP = "pref.App"

    enum class Key(val value: String) {
        AppToken("key.appToken")
    }

    fun save(context: Context?, key: Key, value: Any) {
        val prefs = context?.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE) ?: return
        with(prefs.edit()) {
            (value as? String)?.let { putString(key.value, it) }
            (value as? Int)?.let { putInt(key.value, it) }
            (value as? Boolean)?.let { putBoolean(key.value, it) }
            (value as? Long)?.let { putLong(key.value, it) }
            (value as? Float)?.let { putFloat(key.value, it) }
            apply()
        }
    }

    fun has(context: Context?, key: Key): Boolean {
        val prefs = context?.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE) ?: return false
        return prefs.contains(key.value)
    }

    fun get(context: Context?, key: Key, type: Any): Any? {
        val prefs = context?.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE) ?: return null
        if (!has(context, key)) {
            return null
        }
        if (type is String.Companion) {
            return prefs.getString(key.value, null)
        }
        if (type is Int.Companion) {
            return prefs.getInt(key.value, 0)
        }
        if (type is Boolean.Companion) {
            return prefs.getBoolean(key.value, false)
        }
        if (type is Long.Companion) {
            return prefs.getLong(key.value, 0L)
        }
        if (type is Float.Companion) {
            return prefs.getFloat(key.value, 0F)
        }
        return null
    }

    override fun dispose() {
    }
}