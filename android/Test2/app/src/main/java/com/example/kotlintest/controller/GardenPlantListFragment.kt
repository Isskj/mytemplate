package com.example.kotlintest.controller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.kotlintest.controller.adapter.GardenListAdapter
import com.example.kotlintest.databinding.FragmentGardenListBinding
import com.example.kotlintest.model.GardenPlanting
import com.example.kotlintest.model.Plant
import com.example.kotlintest.module.PlantModule
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


class GardenPlantListFragment : BaseFragment(), GardenListAdapter.OnItemClickListener {

    private val disposal = CompositeDisposable()
    private lateinit var binding: FragmentGardenListBinding
    private val plantModule = PlantModule()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGardenListBinding.inflate(inflater, container, false)
        binding.gardenList.adapter = GardenListAdapter(this)
        binding.addPlant.setOnClickListener {
            coordinator?.pagePlantList()
        }
        subscribe()
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposal.clear()
    }

    override fun onItemClick(plant: Plant?) {
        plant?.let {
            coordinator?.pagePlantDetail(plant.plantId)
        }
    }

    private fun subscribe() {
        plantModule.getGardenPlants(object : SingleObserver<List<GardenPlanting>>{
            override fun onSuccess(it: List<GardenPlanting>) {
                (binding.gardenList.adapter as? GardenListAdapter)?.submitList(it)
                binding.hasPlantings = it.isNotEmpty()
            }
            override fun onSubscribe(d: Disposable) {
                disposal.add(d)
            }
            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        })
    }
}
