package com.example.kotlintest.controller

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlintest.R
import com.example.kotlintest.controller.adapter.HomeViewPagerAdapter
import com.example.kotlintest.databinding.FragmentViewPagerBinding
import com.google.android.material.tabs.TabLayoutMediator


class HomeViewPagerFragment : BaseFragment() {

    companion object {
        const val GARDEN_LIST = 0
        const val PLANT_LIST = 1
    }

    private lateinit var binding: FragmentViewPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        binding.viewPager.adapter = HomeViewPagerAdapter(this)
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.setIcon(getTabIconResource(position))
            tab.text = getTabText(position)
        }.attach()
        return binding.root
    }

    fun selectFragment(num: Int) {
        binding.viewPager.setCurrentItem(num, true)
    }

    private fun getTabIconResource(position: Int): Int {
        return when (position) {
            GARDEN_LIST -> R.drawable.garden_tab_selector
            PLANT_LIST -> R.drawable.plant_list_tab_selector
            else -> throw IndexOutOfBoundsException()
        }
    }

    private fun getTabText(position: Int): String? {
        return when (position) {
            GARDEN_LIST -> getString(R.string.my_garden_title)
            PLANT_LIST -> getString(R.string.plant_list_title)
            else -> throw IndexOutOfBoundsException()
        }
    }
}