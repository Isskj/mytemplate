package com.example.kotlintest.controller

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.kotlintest.MainApplication
import com.example.kotlintest.coordinator.AppCoordinator

open class BaseFragment : Fragment() {

    val coordinator: AppCoordinator?
        get() = (activity?.application as? MainApplication)?.appCoordinator

    override fun onDestroy() {
        coordinator?.dispose(this)
        super.onDestroy()
    }

    override fun onDestroyView() {
        (view as? ViewGroup)?.removeAllViews()
        super.onDestroyView()
    }
}