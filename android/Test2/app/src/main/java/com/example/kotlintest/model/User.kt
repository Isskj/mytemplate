package com.example.kotlintest.model

data class User(
    val email: String,
    val password: String,
    val name: String? = null,
    val age: Int? = null,
    val token: String? = null,
    val refreshToken: String? = null
)
