package com.example.kotlintest.coordinator

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.kotlintest.MainActivity
import com.example.kotlintest.R
import com.example.kotlintest.controller.HomeViewPagerFragment
import com.example.kotlintest.controller.HomeViewPagerFragment.Companion.PLANT_LIST
import com.example.kotlintest.controller.PlantDetailFragment
import com.example.kotlintest.module.SavedStateComponent
import java.lang.ref.WeakReference

class AppCoordinator {

    val stateComponent = SavedStateComponent()
    private var activity: WeakReference<MainActivity>? = null
    private var cachedFragments: MutableList<WeakReference<Fragment>> = mutableListOf()

    fun start(activity: MainActivity, bundle: Bundle?) {
        this.activity = WeakReference(activity)
        activity.setContentView(R.layout.activity_main)
        pageTop(bundle)
        stateComponent.callback = object : SavedStateComponent.Callback {
            override fun onSaved(bundle: Bundle?) {
            }

            override fun onRestored(bundle: Bundle?) {
                stateComponent.state["plantId"]?.let {
                    pagePlantDetail(it)
                }
            }
        }
    }

    fun pageTop(bundle: Bundle?) {
        activity?.get()?.supportFragmentManager?.apply {
            val fragment = cachedFragments.firstOrNull {
                it.get() is HomeViewPagerFragment
            }?.get() ?: HomeViewPagerFragment()
            cachedFragments.add(WeakReference(fragment))
            fragment.arguments = bundle
            beginTransaction()
                .replace(
                    R.id.fragment_container, fragment,
                    HomeViewPagerFragment::class.simpleName
                )
                .commitAllowingStateLoss()
        }
    }

    fun pagePlantList() {
        (cachedFragments.firstOrNull {
            it.get() is HomeViewPagerFragment
        }?.get() as? HomeViewPagerFragment)?.selectFragment(PLANT_LIST)
    }

    fun pagePlantDetail(plantId: String) {
        activity?.get()?.supportFragmentManager?.apply {
            val fragment = cachedFragments.firstOrNull {
                it.get() is PlantDetailFragment
            }?.get() ?: PlantDetailFragment()
            cachedFragments.add(WeakReference(fragment))
            fragment.arguments = Bundle().apply {
                putString("plantId", plantId)
            }
            beginTransaction()
                .setCustomAnimations(
                    R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right
                )
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
        }
    }

    fun pageBack() {
        activity?.get()?.supportFragmentManager?.popBackStack()
    }

    fun dispose(fragment: Fragment) {
        val list = cachedFragments.filter { it.get() != null || it.get() == fragment }
        cachedFragments.clear()
        cachedFragments.addAll(list)
    }
}
