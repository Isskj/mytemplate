package com.example.kotlintest

import android.app.Application
import com.example.kotlintest.coordinator.AppCoordinator
import com.example.kotlintest.module.APIComponent
import com.example.kotlintest.module.PlantModule
import io.realm.Realm


class MainApplication : Application() {

    val appCoordinator = AppCoordinator()
    private val plantModule: PlantModule = PlantModule()

    override fun onCreate() {
        super.onCreate()
        Realm.init(applicationContext)
        APIComponent.setup(applicationContext)
        plantModule.addDefaultPlantsIfNeeded(applicationContext) {
            plantModule.dispose()
        }
    }
}