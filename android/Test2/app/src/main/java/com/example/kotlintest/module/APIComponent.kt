package com.example.kotlintest.module

import android.content.Context
import android.util.Log
import com.example.kotlintest.R
import com.example.kotlintest.model.User
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST


class APIComponent: BaseModule {

    companion object {
        var apiBaseURL = ""
        fun setup(context: Context) {
            apiBaseURL = context.getString(R.string.api_base_url)
        }

        private val retrofit: Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(apiBaseURL)
                .client(
                    OkHttpClient.Builder().addInterceptor(
                        HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }).build()
                )
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }

        val authAPI: AuthAPI by lazy {
            retrofit.create(AuthAPI::class.java)
        }
    }

    interface AuthAPI {
        @POST("api/auth/signup")
        fun signUp(@Body user: User): Observable<User>

        @POST("api/auth/login")
        fun login(@Body user: User): Observable<User>

        @POST("api/auth/logout")
        fun logout(@Header("Authorization") token: String): Observable<User>

        @POST("api/auth/resign")
        fun resign(@Body user: User): Observable<User>
    }

    override fun dispose() {
    }
}

