package com.example.kotlintest.model

import com.example.kotlintest.module.RealmComponent
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class GardenPlanting(
    @PrimaryKey
    var pid: String = UUID.randomUUID().toString(),
    var plant: Plant? = null,
    var plantDate: String = "",
    var lastWateringDate: String = RealmComponent.dateString(Calendar.getInstance().time)
) : RealmObject()
