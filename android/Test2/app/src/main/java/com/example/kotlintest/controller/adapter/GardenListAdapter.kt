package com.example.kotlintest.controller.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlintest.databinding.ListItemGardenPlantBinding
import com.example.kotlintest.model.GardenPlanting
import com.example.kotlintest.model.Plant

class GardenListAdapter(
    private val itemClickListener: OnItemClickListener
) : ListAdapter<GardenPlanting, RecyclerView.ViewHolder>(DiffCallback()) {

    interface OnItemClickListener {
        fun onItemClick(plant: Plant?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GardenViewHolder(
            ListItemGardenPlantBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GardenViewHolder).bind(getItem(position), itemClickListener)
    }

    private class GardenViewHolder(private val binding: ListItemGardenPlantBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: GardenPlanting, clickListener: OnItemClickListener) {
            itemView.setOnClickListener {
                clickListener.onItemClick(item.plant)
            }
            binding.apply {
                plant = item
                executePendingBindings()
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<GardenPlanting>() {
        override fun areItemsTheSame(oldItem: GardenPlanting, newItem: GardenPlanting): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: GardenPlanting, newItem: GardenPlanting): Boolean {
            return false
        }
    }
}
