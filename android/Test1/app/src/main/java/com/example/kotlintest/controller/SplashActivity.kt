package com.example.kotlintest.controller

import android.os.Bundle
import com.example.kotlintest.R
import com.example.kotlintest.module.StorageModule
import com.example.kotlintest.module.logic.DownloaderModule

class SplashActivity : BaseActivity() {

    private val downloadModule = DownloaderModule()
    private val storageModule = StorageModule()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        this.downloadModule.start().subscribe({
            if (this.storageModule.has(StorageModule.Key.AppToken)) {
                this.coordinator.startTopActivity()
            } else {
                this.coordinator.startUserAuthActivity()
            }
        }, {
            this.coordinator.startUserAuthActivity()
        }).let { super.bag.add(it) }
    }
}
