package com.example.kotlintest.module.system

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

class AndroidPermissionModule {

    var permissionResult: ((Boolean) -> Unit)? = null
    var permissionAsking: (() -> Unit)? = null
    private val requestCode = 100

    fun requestPermissions(activity: Activity, permissions: Array<String>) {
        val notGranted = permissions.filter {
            ContextCompat.checkSelfPermission(
                activity.applicationContext,
                it
            ) != PackageManager.PERMISSION_GRANTED
        }
        if (notGranted.isEmpty()) {
            permissionResult?.invoke(true)
            return
        }
        activity.requestPermissions(notGranted.toTypedArray(), requestCode)
    }

    fun requestPermission(activity: Activity, permission: String) {
        when {
            ContextCompat.checkSelfPermission(
                activity.applicationContext,
                permission
            ) == PackageManager.PERMISSION_GRANTED -> {
                permissionResult?.invoke(true)
            }
            activity.shouldShowRequestPermissionRationale(permission) -> {
                permissionAsking?.invoke()
            }
            else -> {
                activity.requestPermissions(arrayOf(permission), requestCode)
            }
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode != this.requestCode || grantResults.isEmpty()) {
            return
        }
        val granted = grantResults.filter { it == PackageManager.PERMISSION_GRANTED }
        permissionResult?.invoke(granted.size == permissions.size)
    }
}