package com.example.kotlintest.coordinator

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleObserver
import com.example.kotlintest.R
import com.example.kotlintest.controller.auth.LoginFragment
import com.example.kotlintest.controller.auth.SignUpFragment
import com.example.kotlintest.controller.auth.UserAuthActivity
import com.example.kotlintest.controller.dialog.MessageFragment
import com.example.kotlintest.controller.gallery.GalleryDetailFragment
import com.example.kotlintest.controller.gallery.GalleryFragment
import com.example.kotlintest.controller.top.TopActivity
import com.example.kotlintest.controller.top.TopFragment
import com.example.kotlintest.module.system.LifecycleModule
import com.example.kotlintest.module.system.Logger
import retrofit2.HttpException
import java.lang.ref.WeakReference


class AppCoordinator(activity: FragmentActivity) : FragmentManager.FragmentLifecycleCallbacks(),
    LifecycleObserver {

    private val activityRef = WeakReference<FragmentActivity>(activity)
    private val lifecycleModule = LifecycleModule(activity)

    fun showMessage(title: String?, message: String?, completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val mgr = act.supportFragmentManager
            val tag = MessageFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach message fragment: $it")
                completion?.invoke()
            }
            MessageFragment.createMessageFragment(title, message).show(mgr, tag)
        }
    }

    fun showError(error: Throwable, completion: (() -> Unit)? = null) {
        error.printStackTrace()
        (error as? HttpException)?.let { httpException ->
            httpException.response()?.errorBody()?.let { errorResponse ->
                showMessage(null, errorResponse.string(), completion)
            } ?: run {
                showMessage(null, "インターネット接続に失敗しました。通信環境をご確認ください。", completion)
            }
        } ?: run {
            showMessage(null, error.localizedMessage, completion)
        }
    }

    fun startUserAuthActivity(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val intent = Intent(act.applicationContext, UserAuthActivity::class.java)
            val tag = UserAuthActivity.TAG
            lifecycleModule.addStopActivityCallback(act) {
                Logger.d("stop activity: $it")
                completion?.invoke()
            }
            act.startActivity(intent)
            act.finish()
        }
    }

    fun pageLogin(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val transaction = act.supportFragmentManager.beginTransaction()
            val tag = LoginFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach page login: $it")
                completion?.invoke()
            }
            transaction
                .replace(R.id.fragment_container, LoginFragment.create(), tag)
                .commit()
        }
    }

    fun pageSignUp(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val transaction = act.supportFragmentManager.beginTransaction()
            val tag = SignUpFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach page signUp: $it")
                completion?.invoke()
            }
            transaction
                .replace(R.id.fragment_container, SignUpFragment.create(), tag)
                .commit()
        }
    }

    fun startTopActivity(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val intent = Intent(act.applicationContext, TopActivity::class.java)
            lifecycleModule.addStopActivityCallback(act) {
                Logger.d("stop top activity: $it")
                completion?.invoke()
            }
            act.startActivity(intent)
            act.finish()
        }
    }

    fun pageTop(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val transaction = act.supportFragmentManager.beginTransaction()
            val tag = TopFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach top fragment: $it")
                completion?.invoke()
            }
            transaction
                .replace(R.id.fragment_container, TopFragment.create(), tag)
                .commit()
        }
    }

    fun pageGallery(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val transaction = act.supportFragmentManager.beginTransaction()
            val tag = GalleryFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach gallery fragment: $it")
                completion?.invoke()
            }
            transaction
                .replace(R.id.fragment_container, GalleryFragment.create(), tag)
                .commit()
        }
    }

    fun pageGalleryDetail(path: String, completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val transaction = act.supportFragmentManager.beginTransaction()
            val tag = GalleryDetailFragment.TAG
            lifecycleModule.addAttachFragmentCallback(tag) {
                Logger.d("attach gallery detail: $it")
                completion?.invoke()
            }
            transaction
                .setCustomAnimations(
                    R.anim.transition_fadein,
                    R.anim.transition_fadeout,
                    R.anim.transition_fadein,
                    R.anim.transition_fadeout
                )
                .add(android.R.id.content, GalleryDetailFragment.create(path), tag)
                .addToBackStack(null)
                .commit()
        }
    }

    fun dismissGalleryDetail(completion: (() -> Unit)? = null) {
        activityRef.get()?.let { act ->
            val mgr = act.supportFragmentManager
            val tag = GalleryDetailFragment.TAG
            lifecycleModule.addDetachFragmentCallback(tag) {
                Logger.d("detach gallery detail:  $it")
                completion?.invoke()
            }
            mgr.popBackStack()
        }
    }
}