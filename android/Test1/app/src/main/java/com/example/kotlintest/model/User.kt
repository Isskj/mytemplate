package com.example.kotlintest.model

data class User(
    var email: String? = null,
    var password: String? = null,
    var name: String? = null,
    var age: Int? = null,
    var token: String? = null,
    var refreshToken: String? = null
)
