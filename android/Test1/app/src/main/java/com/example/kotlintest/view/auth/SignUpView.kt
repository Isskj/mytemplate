package com.example.kotlintest.view.auth

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.example.kotlintest.databinding.PartsSignupBinding
import com.example.kotlintest.model.User

class SignUpView(
    context: Context,
    attrs: AttributeSet
) : FrameLayout(context, attrs) {

    interface SignUpViewListener {
        fun onTapSignUp(user: User, completion: () -> Unit)
        fun onTapLogin(completion: () -> Unit)
    }

    private val binding: PartsSignupBinding =
        PartsSignupBinding.inflate(LayoutInflater.from(context), this, true).apply {
            signUpView = this@SignUpView
        }

    var listener: SignUpViewListener? = null

    fun onTapSignUp(view: View) {
        listener?.let {
            val user = User(
                email = binding.idEdittext.text.toString(),
                password = binding.passwordEdittext.text.toString(),
                age = binding.ageEdittext.text.toString().toIntOrNull(),
                name = binding.nameEdittext.text.toString()
            )
            view.isEnabled = false
            it.onTapSignUp(user) {
                view.isEnabled = true
            }
        }
    }

    fun onTapLogin(view: View) {
        listener?.let {
            view.isEnabled = false
            it.onTapLogin {
                view.isEnabled = true
            }
        }
    }
}