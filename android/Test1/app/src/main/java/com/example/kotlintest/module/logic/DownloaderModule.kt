package com.example.kotlintest.module.logic

import io.reactivex.Single
import java.util.concurrent.TimeUnit

class DownloaderModule {

    fun start(): Single<Boolean> {
        return Single.just(true).delay(1, TimeUnit.SECONDS)
    }
}