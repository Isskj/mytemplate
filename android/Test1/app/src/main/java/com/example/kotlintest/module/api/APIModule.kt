package com.example.kotlintest.module.api

import com.example.kotlintest.MainApplication
import com.example.kotlintest.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class APIModule {

    companion object {
        private val retrofit: Retrofit by lazy {
            val baseURL = MainApplication.context!!.getString(R.string.api_base_url)
            Retrofit.Builder()
                .baseUrl(baseURL)
                .client(
                    OkHttpClient.Builder().addInterceptor(
                        HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }).build()
                )
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }

        val authAPI: AuthAPI by lazy {
            retrofit.create(
                AuthAPI::class.java
            )
        }
        val authAPIStub: AuthAPI by lazy {
            AuthAPIStub()
        }
    }
}

