package com.example.kotlintest.module.system

import android.content.Context
import android.graphics.Bitmap
import android.provider.MediaStore
import com.bumptech.glide.Glide
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File

class GalleryModule {

    data class Content(var uri: String = "", var name: String = "")

    fun load(context: Context?): Single<List<Content>> {
        return Single.create<List<Content>> { emitter ->
            val projection: Array<String>? = arrayOf(
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.DATA
            )
            val list = mutableListOf<Content>()
            val cursor = context?.contentResolver?.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null
            )
            cursor?.let { c ->
                if (c.moveToFirst()) {
                    do {
                        val content = Content()
                        content.name =
                            c.getString(c.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME))
                        content.uri = c.getString(c.getColumnIndex(MediaStore.Images.Media.DATA))
                        list.add(content)
                    } while (c.moveToNext())
                }
            }
            cursor?.close()
            emitter.onSuccess(list)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadImage(context: Context?, path: String): Single<Bitmap> {
        return Single.create { emitter ->
            context?.let {
                val bmp = Glide.with(it)
                    .asBitmap()
                    .load(File(path))
                    .submit()
                    .get()
                emitter.onSuccess(bmp)
            } ?: run {
                emitter.onError(Throwable("context is null"))
            }
        }
    }
}