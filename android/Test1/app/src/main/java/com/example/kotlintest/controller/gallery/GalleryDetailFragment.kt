package com.example.kotlintest.controller.gallery

import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.example.kotlintest.controller.BaseFragment
import com.example.kotlintest.databinding.FragmentGalleryDetailBinding
import com.example.kotlintest.module.system.GalleryModule
import com.example.kotlintest.module.system.Logger
import com.example.kotlintest.view.common.FlickableViewAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class GalleryDetailFragment : BaseFragment() {

    companion object {
        const val TAG = "GalleryDetailFragment"
        private const val BUNDLE_KEY_IMAGE_PATH = "bundle.key.imagePath"
        fun create(imagePath: String): GalleryDetailFragment {
            return GalleryDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(BUNDLE_KEY_IMAGE_PATH, imagePath)
                }
            }
        }
    }

    private lateinit var binding: FragmentGalleryDetailBinding
    private val galleryModule = GalleryModule()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGalleryDetailBinding.inflate(inflater, container, false)
        val adapter = FlickableViewAdapter().apply {
            onSlide = { direction ->
                when (direction) {
                    FlickableViewAdapter.FLAG_SLIDE_TOP,
                    FlickableViewAdapter.FLAG_SLIDE_BOTTOM -> {
                        coordinator.dismissGalleryDetail {
                            Logger.d("dismissed gallery detail by flicking")
                        }
                    }
                    else -> {
                    }
                }
            }
            attach(binding.imageView)
            addFlag(FlickableViewAdapter.FLAG_SLIDE_TOP)
            addFlag(FlickableViewAdapter.FLAG_SLIDE_BOTTOM)
        }
        val imagePath = arguments?.getString(BUNDLE_KEY_IMAGE_PATH) ?: ""
        binding.apply {
            galleryModule.loadImage(context, imagePath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    imageView.setImage(ImageSource.bitmap(it))
                }, {
                    it.printStackTrace()
                }).addTo(bag)
            var firstScale = 0f
            imageView.setOnStateChangedListener(object :
                SubsamplingScaleImageView.OnStateChangedListener {
                override fun onScaleChanged(newScale: Float, origin: Int) {
                    if (firstScale == 0f) {
                        firstScale = newScale
                    }
                    adapter.enableFlicker((newScale - firstScale) < 0.1f)
                }

                override fun onCenterChanged(newCenter: PointF?, origin: Int) {
                }
            })
            closeButton.setOnClickListener {
                coordinator.dismissGalleryDetail {
                    Logger.d("dismissed gallery detail by close button")
                }
            }
        }
        return binding.root
    }
}
