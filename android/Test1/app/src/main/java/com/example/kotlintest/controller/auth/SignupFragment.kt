package com.example.kotlintest.controller.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import com.example.kotlintest.controller.BaseFragment
import com.example.kotlintest.databinding.FragmentSignupBinding
import com.example.kotlintest.model.User
import com.example.kotlintest.module.logic.UserAuthModule
import com.example.kotlintest.module.system.AndroidSystemModule
import com.example.kotlintest.view.auth.SignUpView
import io.reactivex.rxkotlin.addTo


class SignUpFragment : BaseFragment(), SignUpView.SignUpViewListener {

    companion object {
        const val TAG = "SignUpFragment"
        fun create(): SignUpFragment {
            return SignUpFragment()
        }
    }

    private lateinit var binding: FragmentSignupBinding
    private val userAuth = UserAuthModule()
    private val systemModule = AndroidSystemModule()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignupBinding.inflate(inflater, container, false)
        binding.apply {
            signUpView.listener = this@SignUpFragment
        }
        bindEvent()
        return binding.root
    }

    private fun bindEvent() {
        userAuth.loading
            .subscribe {
                binding.progress.isGone = it.not()
            }.addTo(bag)
    }

    override fun onTapSignUp(user: User, completion: () -> Unit) {
        systemModule.hideKeyboard(context, binding.signUpView)
        userAuth.createAccount(user)
            .subscribe({
                coordinator.startTopActivity()
                completion()
            }, { error ->
                coordinator.showError(error)
                completion()
            }).addTo(bag)
    }

    override fun onTapLogin(completion: () -> Unit) {
        coordinator.pageLogin()
        completion()
    }
}
