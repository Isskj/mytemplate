package com.example.kotlintest.module.logic

import com.example.kotlintest.model.User
import com.example.kotlintest.module.StorageModule
import com.example.kotlintest.module.api.APIModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class UserAuthModule {

    var api = APIModule.authAPIStub
    val loading: PublishSubject<Boolean> = PublishSubject.create()
    private val storageModule = StorageModule()
    private val validatorModule = ValidatorModule()

    fun createAccount(user: User): Single<User> {
        return this.validatorModule
            .dispose()
            .email(user.email)
            .password(user.password)
            .validate()
            .doOnSubscribe {
                this.loading.onNext(true)
            }
            .flatMap {
                this.api.signUp(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            .flatMap {
                it.token?.let { token ->
                    this.storageModule.save(StorageModule.Key.AppToken, token)
                    Single.just(it)
                } ?: run {
                    Single.error<User>(Throwable("token is empty"))
                }
            }
            .doFinally {
                this.loading.onNext(false)
            }
    }

    fun login(user: User): Single<Boolean> {
        return this.validatorModule
            .dispose()
            .email(user.email)
            .password(user.password)
            .validate()
            .doOnSubscribe {
                this.loading.onNext(true)
            }
            .flatMap {
                this.api.login(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            .flatMap {
                it.token?.let { token ->
                    this.storageModule.save(StorageModule.Key.AppToken, token)
                    Single.just(true)
                } ?: run {
                    Single.error<Boolean>(Exception("token is empty"))
                }
            }
            .doFinally {
                this.loading.onNext(false)
            }
    }

    fun logout(): Single<User> {
        this.loading.onNext(true)
        val token = (this.storageModule.get(StorageModule.Key.AppToken, String) as? String) ?: ""
        return this.api.logout("Bearer $token")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                this.storageModule.remove(StorageModule.Key.AppToken)
            }
            .doFinally {
                this.loading.onNext(false)
            }
    }
}