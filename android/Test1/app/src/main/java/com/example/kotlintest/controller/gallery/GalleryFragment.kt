package com.example.kotlintest.controller.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kotlintest.controller.BaseFragment
import com.example.kotlintest.databinding.FragmentGalleryBinding
import com.example.kotlintest.module.system.GalleryModule
import io.reactivex.rxkotlin.addTo

class GalleryFragment : BaseFragment(), GalleryAdapter.GalleryListener {

    companion object {
        const val TAG = "GalleryFragment"
        fun create(): GalleryFragment {
            return GalleryFragment()
        }
    }

    private lateinit var binding: FragmentGalleryBinding
    private val galleryModule = GalleryModule()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGalleryBinding.inflate(inflater, container, false)
        binding.apply {
            recyclerview.layoutManager =
                GridLayoutManager(inflater.context, 3, GridLayoutManager.VERTICAL, false)
            recyclerview.adapter = GalleryAdapter().apply {
                listener = this@GalleryFragment
            }
        }
        loadGallery()
        return binding.root
    }

    private fun loadGallery() {
        galleryModule.load(context)
            .subscribe({ contents ->
                (binding.recyclerview.adapter as? GalleryAdapter)?.let {
                    it.urls.addAll(contents.map { it.uri })
                    it.notifyDataSetChanged()
                }
            }, {
                coordinator.showError(it)
            }).addTo(bag)
    }

    override fun onGalleryItemClicked(url: String, completion: () -> Unit) {
        coordinator.pageGalleryDetail(url)
        completion()
    }
}