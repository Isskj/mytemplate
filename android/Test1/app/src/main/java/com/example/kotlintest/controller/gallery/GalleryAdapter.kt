package com.example.kotlintest.controller.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.example.kotlintest.R
import java.io.File

class GalleryAdapter : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    interface GalleryListener {
        fun onGalleryItemClicked(url: String, completion: () -> Unit)
    }

    val urls = mutableListOf<String>()

    var listener: GalleryListener? = null

    class ViewHolder(val imageView: ImageView) : RecyclerView.ViewHolder(imageView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.gallery_list_item, parent, false) as ImageView
        view.layoutParams.apply {
            width = parent.measuredWidth / 3
            height = parent.measuredWidth / 3
        }
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return urls.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.setOnClickListener { view ->
            listener?.let {
                view.isEnabled = false
                it.onGalleryItemClicked(urls[position]) {
                    view.isEnabled = true
                }
            }
        }
        Glide.with(holder.imageView)
            .load(File(urls[position]))
            .transition(withCrossFade())
            .centerCrop()
            .into(holder.imageView)
    }
}