package com.example.kotlintest.controller

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.kotlintest.coordinator.AppCoordinator
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {

    val bag = CompositeDisposable()
    lateinit var coordinator: AppCoordinator

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.coordinator = (activity as BaseActivity).coordinator
    }

    override fun onDestroyView() {
        bag.dispose()
        super.onDestroyView()
    }
}