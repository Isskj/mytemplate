package com.example.kotlintest.controller.top

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import com.example.kotlintest.controller.BaseFragment
import com.example.kotlintest.databinding.FragmentTopBinding
import com.example.kotlintest.module.logic.UserAuthModule
import io.reactivex.rxkotlin.addTo

class TopFragment : BaseFragment() {

    companion object {
        const val TAG = "TopFragment"
        fun create(): TopFragment {
            return TopFragment()
        }
    }

    private lateinit var binding: FragmentTopBinding
    private val userAuth = UserAuthModule()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.binding = FragmentTopBinding.inflate(inflater, container, false).apply {
            logoutButton.setOnClickListener {
                userAuth.logout()
                    .subscribe({
                        coordinator.startUserAuthActivity()
                    }, {
                        coordinator.startUserAuthActivity()
                    }).addTo(bag)
            }
            userAuth.loading.subscribe {
                progress.isGone = it.not()
            }.addTo(bag)
        }
        return binding.root
    }
}
