package com.example.kotlintest.module.system

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

class AndroidSystemModule {

    fun hideKeyboard(context: Context?, view: View?) {
        (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}