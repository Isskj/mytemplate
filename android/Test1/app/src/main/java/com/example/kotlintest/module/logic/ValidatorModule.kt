package com.example.kotlintest.module.logic

import io.reactivex.Single

class ValidatorModule {

    class ValidationError : Throwable("error.validation") {
        var causes = mutableListOf<Throwable>()
        fun hasError(): Boolean {
            return causes.size > 0
        }
    }

    private var error = ValidationError()

    fun email(email: String?): ValidatorModule {
        email?.let {
            val pattern = "^.+@.+\\..+$"
            val result = pattern.toRegex().matches(email)
            if (!result) {
                this.error.causes.add(Throwable("email_invalid"))
            }
        } ?: run {
            this.error.causes.add(Throwable("email_empty"))
        }
        return this
    }

    fun password(password: String?): ValidatorModule {
        if (password == null || password.isEmpty()) {
            this.error.causes.add(Throwable("password_empty"))
        }
        return this
    }

    fun validate(): Single<Boolean> {
        if (this.error.hasError()) {
            return Single.error<Boolean>(this.error.causes.first())
        }
        return Single.just(true)
    }

    fun dispose(): ValidatorModule {
        this.error = ValidationError()
        return this
    }
}