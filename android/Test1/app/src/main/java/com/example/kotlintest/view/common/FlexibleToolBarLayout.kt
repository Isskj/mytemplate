package com.example.kotlintest.view.common

import android.content.Context
import android.util.AttributeSet
import android.util.Range
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ScrollView
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

class FlexibleToolBarLayout(
    context: Context,
    attrs: AttributeSet
) : FrameLayout(context, attrs) {

    var toolBarLayoutId: Int = 0
    var toolBarId: Int = 0
    var contentId: Int = 0

    private lateinit var toolBarLayout: View
    private lateinit var toolBar: View
    private lateinit var contentView: View
    private var toolBarHeight = 0
    private var contentViewOffsetTopLimit = 0
    private var toolBarRangeY = Range(0, 0)
    private var contentViewRangeY = Range(0, 0)

    fun setupLayout() {
        if (contentId == 0 || toolBarLayoutId == 0 || toolBarId == 0) {
            return
        }
        contentView = findViewById<View>(contentId)
        toolBarLayout = findViewById<View>(toolBarLayoutId)
        toolBar = findViewById<View>(toolBarId)
        toolBarLayout.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (toolBarHeight == 0) {
                contentViewOffsetTopLimit = v.paddingTop
                toolBarHeight = bottom - top
                toolBarRangeY = Range(-toolBarHeight + contentViewOffsetTopLimit, 0)
                contentViewRangeY = Range(contentViewOffsetTopLimit, toolBarHeight)
            }
        }
        setupSiblingViews(contentView)
    }

    private fun setupSiblingViews(root: View) {
        if (root is ViewGroup) {
            root.children.forEach { child ->
                when (child) {
                    is RecyclerView -> setupRecyclerView(child)
                    is ScrollView -> setupScrollView(child)
                    else -> setupSiblingViews(child)
                }
            }
        }
    }

    private fun setupScrollView(view: ScrollView) {
        // TODO
    }

    private fun setupRecyclerView(view: RecyclerView) {
        var offsetY = 0
        val acceleration = 3
        view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                offsetY += dy
                val toolBarOffsetY = -((offsetY).toDouble() / acceleration).toInt()
                if (toolBarRangeY.contains(toolBarOffsetY)) {
                    toolBarLayout.translationY = toolBarOffsetY.toFloat()
                    val alpha =
                        1f - Math.abs(toolBarOffsetY.toFloat() / (toolBarHeight - contentViewOffsetTopLimit).toFloat())
                    toolBar.alpha = alpha
                }
                val contentViewOffsetY = toolBarOffsetY + toolBarHeight
                if (contentViewRangeY.contains(contentViewOffsetY)) {
                    contentView.translationY = contentViewOffsetY.toFloat()
                }
                if (toolBarOffsetY == 0) {
                    toolBarLayout.translationY = toolBarRangeY.upper.toFloat()
                    contentView.translationY = contentViewRangeY.upper.toFloat()
                    toolBar.alpha = 1f
                } else if (contentViewOffsetY < contentViewOffsetTopLimit) {
                    toolBarLayout.translationY = toolBarRangeY.lower.toFloat()
                    contentView.translationY = contentViewRangeY.lower.toFloat()
                    toolBar.alpha = 0f
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }
}

@BindingAdapter("flexibleToolBarLayout", "flexibleToolBar", "flexibleContent")
fun FlexibleToolBarLayout.setToolBarLayout(toolBarLayoutId: Int, toolBarId: Int, contentId: Int) {
    this.toolBarLayoutId = toolBarLayoutId
    this.toolBarId = toolBarId
    this.contentId = contentId
    setupLayout()
}
