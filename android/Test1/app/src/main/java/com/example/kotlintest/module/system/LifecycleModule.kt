package com.example.kotlintest.module.system

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner


class LifecycleModule(
    activity: FragmentActivity
) : LifecycleEventObserver, FragmentManager.FragmentLifecycleCallbacks() {

    enum class Event {
        AttachFragment, DetachFragment, StopActivity
    }

    inner class EventCallback<T>(val type: Event, val tag: Any?, val completion: ((T) -> Unit)?)

    private val callbacks = mutableListOf<EventCallback<*>>()

    init {
        activity.lifecycle.addObserver(this)
        activity.supportFragmentManager.registerFragmentLifecycleCallbacks(this, false)
    }

    fun addAttachFragmentCallback(tag: String, completion: ((Fragment) -> Unit)?) {
        callbacks.add(EventCallback(Event.AttachFragment, tag, completion))
    }

    fun addDetachFragmentCallback(tag: String, completion: ((Fragment) -> Unit)?) {
        callbacks.add(EventCallback(Event.DetachFragment, tag, completion))
    }

    fun addStopActivityCallback(source: LifecycleOwner, completion: ((LifecycleOwner) -> Unit)?) {
        callbacks.add(EventCallback(Event.StopActivity, source, completion))
    }

    @Suppress("UNCHECKED_CAST")
    override fun onFragmentAttached(fm: FragmentManager, f: Fragment, context: Context) {
        super.onFragmentAttached(fm, f, context)
        callbacks.find { it.tag == f.tag && it.type == Event.AttachFragment }?.let {
            (it as? EventCallback<Fragment>)?.let { callback ->
                callback.completion?.invoke(f)
                callbacks.remove(callback)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onFragmentDetached(fm: FragmentManager, f: Fragment) {
        super.onFragmentDetached(fm, f)
        callbacks.find { it.tag == f.tag && it.type == Event.DetachFragment }?.let {
            (it as? EventCallback<Fragment>)?.let { callback ->
                callback.completion?.invoke(f)
                callbacks.remove(callback)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        callbacks.find { it.tag == source }?.let {
            (it as? EventCallback<LifecycleOwner>)?.let { callback ->
                if (callback.type == Event.StopActivity && event == Lifecycle.Event.ON_STOP) {
                    callback.completion?.invoke(source)
                    callbacks.remove(callback)
                }
            }
        }
    }
}