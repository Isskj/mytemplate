package com.example.kotlintest.controller.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class MessageFragment : DialogFragment() {

    companion object {
        const val TAG = "MessageFragment"
        const val BUNDLE_KEY_TITLE = "bundle.key.title"
        const val BUNDLE_KEY_MESSAGE = "bundle.key.message"
        fun createMessageFragment(title: String?, message: String?): MessageFragment {
            val fragment = MessageFragment()
            fragment.arguments = Bundle().apply {
                putString(BUNDLE_KEY_TITLE, title)
                putString(BUNDLE_KEY_MESSAGE, message)
            }
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val titleValue = arguments?.getString(BUNDLE_KEY_TITLE)
        val messageValue = arguments?.getString(BUNDLE_KEY_MESSAGE)
        activity?.let {
            return AlertDialog.Builder(it)
                .setTitle(titleValue)
                .setMessage(messageValue)
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
        }
        return super.onCreateDialog(savedInstanceState)
    }
}