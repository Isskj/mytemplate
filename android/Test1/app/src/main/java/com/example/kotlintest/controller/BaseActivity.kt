package com.example.kotlintest.controller

import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlintest.R
import com.example.kotlintest.coordinator.AppCoordinator
import io.reactivex.disposables.CompositeDisposable

open class BaseActivity : AppCompatActivity() {
    val bag = CompositeDisposable()
    lateinit var coordinator: AppCoordinator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        overridePendingTransition(R.anim.transition_fadein, R.anim.transition_fadeout)
        this.coordinator = AppCoordinator(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(javaClass.simpleName, "onDestroy")
    }
}