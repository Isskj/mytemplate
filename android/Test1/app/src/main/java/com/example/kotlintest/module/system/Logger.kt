package com.example.kotlintest.module.system

import android.graphics.Bitmap
import android.util.Log

object Logger {

    fun dump(bmp: Bitmap, msg: String = "") {
        Log.d(
            "[Bitmap]",
            "$msg density:${bmp.density} w:${bmp.width} h:${bmp.height} config:${bmp.config} " +
                    "scaledW:${bmp.getScaledWidth(bmp.density)} scaledH:${bmp.getScaledHeight(bmp.density)} " +
                    "hasMipmap:${bmp.hasMipMap()} hasAlpha:${bmp.hasAlpha()}"
        )
    }

    fun d(msg: String?) {
        Log.d("[DEBUG]", "$msg")
    }
}