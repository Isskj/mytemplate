package com.example.kotlintest.view.common

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Path
import android.graphics.Point
import android.graphics.RectF
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.animation.doOnEnd
import com.example.kotlintest.R

class FlickableViewAdapter : View.OnTouchListener {

    companion object {
        const val FLAG_SLIDE_TOP = 1 shl 0
        const val FLAG_SLIDE_BOTTOM = 1 shl 1
        const val FLAG_SLIDE_LEFT = 1 shl 2
        const val FLAG_SLIDE_RIGHT = 1 shl 3
    }

    private lateinit var view: View
    private var closed = false
    private var touchDownX = 0f
    private var touchDownY = 0f
    private var limitRect = RectF()
    private var duration = 0L
    private var flag = 0

    var onSlide: ((Int) -> Unit)? = null

    fun attach(view: View) {
        this.view = view
        val point = Point()
        (view.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
            .defaultDisplay.getSize(point)
        val limitX = point.x / 7f
        val limitY = point.y / 7f
        this.limitRect.set(-limitX, -limitY, limitX, limitY)
        this.duration =
            view.context.resources.getInteger(R.integer.transition_animation_duration).toLong()
        view.setOnTouchListener(this)
    }

    fun addFlag(flag: Int) {
        this.flag = this.flag or flag
    }

    private fun flagEnabled(flag: Int): Boolean {
        return this.flag and flag == flag
    }

    fun enableFlicker(enabled: Boolean) {
        closed = enabled.not()
        slideTo(0f, 0f)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (closed.not()) {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    touchDownX = event.x
                    touchDownY = event.y
                }
                MotionEvent.ACTION_MOVE -> {
                    val dx = (event.x - touchDownX) / 5f
                    val dy = (event.y - touchDownY) / 5f
                    view.translationX += dx
                    view.translationY += dy
                    if (limitRect.contains(view.translationX, view.translationY).not()) {
                        slideOut()
                    }
                }
                MotionEvent.ACTION_UP -> {
                    if (limitRect.contains(view.translationX, view.translationY).not()) {
                        slideOut()
                    } else {
                        slideTo(0f, 0f)
                    }
                }
            }
        }
        return false
    }

    private fun slideTo(x: Float, y: Float, completion: (() -> Unit)? = null) {
        val path = Path()
        path.moveTo(view.translationX, view.translationY)
        path.lineTo(x, y)
        ObjectAnimator.ofFloat(view, "translationX", "translationY", path).apply {
            interpolator = AccelerateDecelerateInterpolator()
            duration = this@FlickableViewAdapter.duration
            doOnEnd {
                completion?.invoke()
            }
            start()
        }
    }

    private fun slideOut() {
        when {
            view.translationX < limitRect.left -> {
                if (flagEnabled(FLAG_SLIDE_LEFT)) {
                    closed = true
                    slideTo(-10000f, view.translationY) {
                        onSlide?.invoke(FLAG_SLIDE_LEFT)
                    }
                } else {
                    slideTo(0f, 0f)
                }
            }
            view.translationX > limitRect.right -> {
                if (flagEnabled(FLAG_SLIDE_RIGHT)) {
                    closed = true
                    slideTo(10000f, view.translationY) {
                        onSlide?.invoke(FLAG_SLIDE_RIGHT)
                    }
                } else {
                    slideTo(0f, 0f)
                }
            }
            view.translationY < limitRect.top -> {
                if (flagEnabled(FLAG_SLIDE_TOP)) {
                    closed = true
                    slideTo(view.translationX, -10000f) {
                        onSlide?.invoke(FLAG_SLIDE_TOP)
                    }
                } else {
                    slideTo(0f, 0f)
                }
            }
            view.translationY > limitRect.bottom -> {
                if (flagEnabled(FLAG_SLIDE_BOTTOM)) {
                    closed = true
                    slideTo(view.translationX, 10000f) {
                        onSlide?.invoke(FLAG_SLIDE_BOTTOM)
                    }
                } else {
                    slideTo(0f, 0f)
                }
            }
        }
    }
}