package com.example.kotlintest.module

import android.content.Context
import android.content.SharedPreferences
import com.example.kotlintest.MainApplication

class StorageModule {
    val PREF_APP = "pref.App"

    enum class Key(val value: String) {
        AppToken("key.appToken")
    }

    fun save(key: Key, value: Any) {
        val prefs = sharedPreference() ?: return
        with(prefs.edit()) {
            (value as? String)?.let { putString(key.value, it) }
            (value as? Int)?.let { putInt(key.value, it) }
            (value as? Boolean)?.let { putBoolean(key.value, it) }
            (value as? Long)?.let { putLong(key.value, it) }
            (value as? Float)?.let { putFloat(key.value, it) }
            apply()
        }
    }

    fun has(key: Key): Boolean {
        val prefs = sharedPreference() ?: return false
        return prefs.contains(key.value)
    }

    fun get(key: Key, type: Any): Any? {
        if (!has(key)) {
            return null
        }
        val prefs = sharedPreference() ?: return null
        if (type is String.Companion) {
            return prefs.getString(key.value, null)
        }
        if (type is Int.Companion) {
            return prefs.getInt(key.value, 0)
        }
        if (type is Boolean.Companion) {
            return prefs.getBoolean(key.value, false)
        }
        if (type is Long.Companion) {
            return prefs.getLong(key.value, 0L)
        }
        if (type is Float.Companion) {
            return prefs.getFloat(key.value, 0F)
        }
        return null
    }

    fun remove(key: Key) {
        if (!has(key)) {
            return
        }
        val prefs = sharedPreference() ?: return
        with(prefs.edit()) {
            remove(key.value).apply()
        }
    }

    private fun sharedPreference(): SharedPreferences? {
        return MainApplication.context?.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE)
    }
}