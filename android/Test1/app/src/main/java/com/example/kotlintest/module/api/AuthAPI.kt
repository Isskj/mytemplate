package com.example.kotlintest.module.api

import com.example.kotlintest.model.User
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.*
import java.util.concurrent.TimeUnit

interface AuthAPI {
    @POST("api/auth/signup")
    fun signUp(@Body user: User): Single<User>

    @POST("api/auth/login")
    fun login(@Body user: User): Single<User>

    @POST("api/auth/logout")
    fun logout(@Header("Authorization") authHeader: String): Single<User>

    @POST("api/auth/resign")
    fun resign(@Body user: User): Single<User>
}

class AuthAPIStub : AuthAPI {
    override fun signUp(user: User): Single<User> {
        val response = User(
            email = user.email,
            password = user.password,
            token = UUID.randomUUID().toString(),
            refreshToken = UUID.randomUUID().toString()
        )
        return Single.just(response).delay(2, TimeUnit.SECONDS)
    }

    override fun login(user: User): Single<User> {
        val response = User(
            email = user.email,
            password = user.password,
            token = UUID.randomUUID().toString(),
            refreshToken = UUID.randomUUID().toString()
        )
        return Single.just(response).delay(2, TimeUnit.SECONDS)
    }

    override fun logout(token: String): Single<User> {
        val response = User(
            token = token,
            refreshToken = UUID.randomUUID().toString()
        )
        return Single.just(response).delay(2, TimeUnit.SECONDS)
    }

    override fun resign(user: User): Single<User> {
        val response = User(
            email = user.email,
            password = user.password,
            token = UUID.randomUUID().toString(),
            refreshToken = UUID.randomUUID().toString()
        )
        return Single.just(response).delay(2, TimeUnit.SECONDS)
    }
}
