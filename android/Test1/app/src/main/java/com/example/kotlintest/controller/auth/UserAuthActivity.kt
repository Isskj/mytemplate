package com.example.kotlintest.controller.auth

import android.os.Bundle
import com.example.kotlintest.R
import com.example.kotlintest.controller.BaseActivity

class UserAuthActivity : BaseActivity() {

    companion object {
        const val TAG = "UserAuthActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_auth)
        coordinator.pageLogin()
    }
}