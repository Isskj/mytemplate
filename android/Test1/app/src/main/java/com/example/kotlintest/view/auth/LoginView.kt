package com.example.kotlintest.view.auth

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.example.kotlintest.databinding.PartsLoginBinding
import com.example.kotlintest.model.User

class LoginView(
    context: Context,
    attrs: AttributeSet
) : FrameLayout(context, attrs) {

    interface LoginViewListener {
        fun onTapLogin(user: User, completion: () -> Unit)
        fun onTapSignUp(completion: () -> Unit)
    }

    private val binding: PartsLoginBinding =
        PartsLoginBinding.inflate(LayoutInflater.from(context), this, true).apply {
            loginView = this@LoginView
        }

    var loginListener: LoginViewListener? = null

    fun tapLogin(view: View) {
        loginListener?.let {
            val user = User(
                email = binding.idEdittext.text.toString(),
                password = binding.passwordEdittext.text.toString()
            )
            view.isEnabled = false
            it.onTapLogin(user) {
                view.isEnabled = true
            }
        }
    }

    fun tapSignUp(view: View) {
        loginListener?.let {
            view.isEnabled = false
            it.onTapSignUp {
                view.isEnabled = true
            }
        }
    }
}
