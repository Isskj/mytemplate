package com.example.kotlintest.controller.top

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import com.example.kotlintest.R
import com.example.kotlintest.controller.BaseActivity
import com.example.kotlintest.databinding.ActivityTopBinding
import com.example.kotlintest.module.logic.UserAuthModule
import com.example.kotlintest.module.system.AndroidPermissionModule
import io.reactivex.rxkotlin.addTo

class TopActivity : BaseActivity() {

    companion object {
        const val TAG = "TopActivity"
    }

    private val permission = AndroidPermissionModule()
    private val userAuth = UserAuthModule()
    private lateinit var binding: ActivityTopBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_top)

        permission.permissionResult = { granted ->
            if (granted) {
                binding.toolbarContainer.setupLayout()
                coordinator.pageGallery()
            }
        }
        permission.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permission.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun onClickLeave(view: View) {
        userAuth.loading.subscribe {
            binding.progress.isGone = it.not()
        }.addTo(bag)
        userAuth.logout()
            .subscribe({
                coordinator.startUserAuthActivity()
            }, {
                coordinator.startUserAuthActivity()
            }).addTo(bag)
    }
}