package com.example.kotlintest

import com.example.kotlintest.model.User
import com.example.kotlintest.module.api.APIModule
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import org.junit.Test

import org.junit.Assert.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testAPIClient() {
        val lock = CountDownLatch(1)
        val user = User(
            email = "test@gmail.com",
            name = "test",
            password = "test",
            age = 30
        )
        APIModule.authAPI.signup(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.also { println(it) }
                lock.countDown();

            }, {
                println(it)
                lock.countDown();
            })
        lock.await(30, TimeUnit.SECONDS)
    }
}
