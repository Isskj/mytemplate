package com.example.kotlintest.module.logic

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch

@RunWith(AndroidJUnit4::class)
class ValidationTest {

    val validatorModule = ValidatorModule()

    @Before
    fun setup() {
    }

    @Test
    fun testEmailOK() {
        val countDownLatch = CountDownLatch(1)
        this.validatorModule.email("test.+1@example.com").validate().subscribe({
            Assert.assertTrue(it)
            countDownLatch.countDown()
        }, { error ->
            Log.e(javaClass.simpleName, error.message)
            countDownLatch.countDown()
        }).dispose()
        countDownLatch.await()
    }

    @Test
    fun testEmailSuccess() {
        checkEmailSuccess("test@example.com")
        checkEmailSuccess("12345@example.com")
        checkEmailSuccess("+12345@example.com")
        checkEmailSuccess("a~+1@example.0")
        checkEmailSuccess("a.-^1@example.1")
    }

    @Test
    fun testEmailFailure() {
        checkEmailFailure(null, ValidatorModule.ValidationError().apply {
            causes.add(Throwable("email_empty"))
        })
        checkEmailFailure("test12345", ValidatorModule.ValidationError().apply {
            causes.add(Throwable("email_invalid"))
        })
        checkEmailFailure("1", ValidatorModule.ValidationError().apply {
            causes.add(Throwable("email_invalid"))
        })
        checkEmailFailure("example.example.com", ValidatorModule.ValidationError().apply {
            causes.add(Throwable("email_invalid"))
        })
    }

    private fun checkEmailSuccess(email: String?) {
        val countDownLatch = CountDownLatch(1)
        this.validatorModule.email(email).validate().subscribe({
            Assert.assertTrue(it)
            countDownLatch.countDown()
        }, {
            Assert.fail(it.message)
            countDownLatch.countDown()
        }).dispose()
        countDownLatch.await()
        this.validatorModule.dispose()
    }

    private fun checkEmailFailure(email: String?, expected: ValidatorModule.ValidationError) {
        val countDownLatch = CountDownLatch(1)
        this.validatorModule.email(email).validate().subscribe({
            countDownLatch.countDown()
        }, {
            (it as? ValidatorModule.ValidationError)?.let { error ->
                Assert.assertEquals(expected.message, error.message)
                for ((index, value) in error.causes.withIndex()) {
                    Assert.assertEquals(expected.causes[index].message, error.causes[index].message)
                }
            } ?: run {
                Assert.fail(it.message)
            }
            countDownLatch.countDown()
        }).dispose()
        countDownLatch.await()
        this.validatorModule.dispose()
    }
}