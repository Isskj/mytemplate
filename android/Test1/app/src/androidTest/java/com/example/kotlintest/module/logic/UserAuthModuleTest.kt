package com.example.kotlintest.module.logic

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.kotlintest.model.User
import com.example.kotlintest.module.api.AuthAPIStub
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.CountDownLatch

@RunWith(AndroidJUnit4::class)
class UserAuthModuleTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val userAuth = UserAuthModule()

    @Before
    fun setup() {
        userAuth.api = AuthAPIStub()
    }

    @Test
    fun testSignUp() {
        val latch = CountDownLatch(1)
        userAuth.loading.subscribe {
            Log.d(javaClass.simpleName, "loading:$it")
        }
        val user = User(email = "${Date().time}@gmail.com", password = "test12345", name = "test name", age = 30)
        userAuth.createAccount(user).subscribe({
            Log.d(javaClass.simpleName, it.toString())
            Assert.assertNotNull(it)
            latch.countDown()
        }, {
            Assert.assertNotNull(it)
            latch.countDown()
        })
        latch.await()
    }

    @Test
    fun testChainingAPIRequest() {
        val latch = CountDownLatch(1)
        userAuth.loading.subscribe {
            Log.d(javaClass.simpleName, "loading:$it")
        }
        val user = User(email = "${Date().time}@gmail.com", password = "test12345", name = "test name", age = 30)
        userAuth.createAccount(user).subscribe({
            userAuth.login(user).subscribe({
                Log.d(javaClass.simpleName, it.toString())
                Assert.assertNotNull(it)
                latch.countDown()
            }, {
                it.printStackTrace()
                Assert.fail(it.message)
                latch.countDown()
            })
            Log.d(javaClass.simpleName, it.toString())
            Assert.assertNotNull(it)
        }, {
            it.printStackTrace()
            Assert.fail(it.message)
        })
        latch.await()
    }
}